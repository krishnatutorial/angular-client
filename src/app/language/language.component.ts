﻿import { Component, OnInit } from '@angular/core';
import { TranslateService } from "./translate.service";

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'language.component.html'
})

export class LanguageComponent  {
    constructor(private translateService: TranslateService) { }

    selectLanguage(str) {
        return this.translateService.selectLanguage(str);
    }
}