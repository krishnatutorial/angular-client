﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../_models/index';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get('http://localhost:2017/private/users');
    }

     getById(id: number) {
        return this.http.get('/api/users/' + id);
    } 

    create(user: User) {
        console.log("user", User);
        return this.http.post('http://localhost:2017/public/user', user);
    }

    update(user: User) {
        return this.http.put('http://localhost:2017/public/user' + user.id, user);
    }

    delete(id: number) {
        return this.http.delete('http://localhost:2017/public/user/' + id);
    } 
}