﻿export class User {
    id: number;
    email: string;
    password: string;
    createdAt?: string;
    updatedAt?: string;
}
export class Item {
    id: number;
    name: string;
    price: string;
    createdAt?: string;
    updatedAt?: string;
}